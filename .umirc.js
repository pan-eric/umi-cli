// ref: https://umijs.org/config/
export default {
  treeShaking: true,
  routes: [
    {
      path: '/',
      component: './home',
      routes: [
        {
          path: '/',
          component: './home/homepage/index.js',
        },
        {
          path: '/movie',
          component: './home/movie/index.js',
        },
        {
          path: '/cinema',
          component: './home/cinema/index.js',
        },
        {
          path: '/show',
          component: './home/show/index.js',
        },
        {
          path: '/list',
          component: './home/list/index.js',
        },
        {
          path: '/hotspot',
          component: './home/hotspot/index.js',
        },
        {
          path: '/shop',
          component: './home/shop/index.js',
        }
      ],
    },
  ],
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
      {
        antd: false,
        dva: false,
        dynamicImport: false,
        title: 'myapp',
        dll: false,
        routes: {
          exclude: [/components\//],
        },
      },
    ],
  ],
};
