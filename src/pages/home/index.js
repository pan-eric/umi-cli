import Link from 'umi/link'
import './index.css';

function BasicLayout(props) {
    return (
    <div>
      <img src="" alt="" />
      <Link to='/'>首页</Link>
      <Link to='/movie'>电影</Link>
      <Link to='/cinema'>影院</Link>
      <Link to='/show'>演出</Link>
      <Link to='/list'>榜单</Link>
      <Link to='/hotspot'>热点</Link>
      <Link to='/shop'>商城</Link>
      {props.children}
   </div>
  );
}

export default BasicLayout;